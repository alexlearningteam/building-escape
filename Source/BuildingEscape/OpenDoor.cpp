// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "OpenDoor.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	if (!PressurePlate) {
		UE_LOG(LogTemp, Error, TEXT("Pressure Plate not set for object: %s"), *GetOwner()->GetName());
	}
}

float UOpenDoor::GetTotalMassOfOverlappingObjects() {
	if (PressurePlate == nullptr) { return 0; }

	float TotalMass = 0.f;
	TArray<AActor*> OverlappingObjects;
	PressurePlate->GetOverlappingActors(OUT OverlappingObjects);

	for (AActor* Actor : OverlappingObjects) {
		UPrimitiveComponent* PrimitiveComponent = Actor->FindComponentByClass<UPrimitiveComponent>();
		TotalMass += PrimitiveComponent->GetMass();
	}

	return TotalMass;
}

// Called every frame
void UOpenDoor::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	if (GetTotalMassOfOverlappingObjects() > MassThreshold) {
		OnOpen.Broadcast();
	}
	else {
		OnClose.Broadcast();
	}
}
