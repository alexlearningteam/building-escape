// Fill out your copyright notice in the Description page of Project Settings.
#define OUT

#include "BuildingEscape.h"
#include "Grabber.h"


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	/// Setup parameters for collision queries. These parameters will
	/// look for PhysicsBody objects only
	ViewPointTracingParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	ViewPointObjectTracingParams = FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody);
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	InitPhysics();
	InitInput();
}

// Fetch Physics Handle component
void UGrabber::InitPhysics() {
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!PhysicsHandle) {
		UE_LOG(LogTemp, Error, TEXT("PhysicsHandle not found: %s"), *GetOwner()->GetName())
	}
	return;
}

// Fetch the InputComponent and bind the Grab actions.
void UGrabber::InitInput() {
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent) {
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Input Component not found: %s"), *GetOwner()->GetName())
	}
	return;
}

// Update the player's viewpoint
void UGrabber::UpdateViewPoint() {
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT ViewPointLocation, 
		OUT ViewPointRotation
	);
	return;
}

// Update the ViewPoint, then return the line trace start
FVector UGrabber::GetLineTraceStart() {
	UpdateViewPoint();
	return ViewPointLocation;
}

// Calculate the line trace end, that is the point the player is looking at
// within the distance established by the Reach attribute.
FVector UGrabber::GetLineTraceEnd() {
	UpdateViewPoint();
	return ViewPointLocation + ViewPointRotation.Vector() * Reach;
}

// Perform a Line Trace using the player's view point and return
// the nearest PhysicsBody
FHitResult UGrabber::GetPointedObject() {
	/// Calculate world coordinates of the View Point vector
	/// (that is, the nearby point the user is looking at).
	FVector LineTraceEnd = GetLineTraceEnd();
	FVector LineTraceStart = GetLineTraceStart();

	/// Get the pointed Actor and return it
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByObjectType(
		OUT HitResult,
		LineTraceStart,
		LineTraceEnd,
		ViewPointObjectTracingParams,
		ViewPointTracingParams
	);

	return HitResult;
}

// Fetch the nearest PhysicsBody within the player's ViewPoint,
// and then attach it to the PhysicsHandle component-
void UGrabber::Grab() {
	FHitResult HitResult = GetPointedObject();
	AActor* PointedActor = HitResult.GetActor();

	if (PointedActor && PhysicsHandle) {
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			HitResult.GetComponent(),
			NAME_None,
			PointedActor->GetActorLocation(),
			PointedActor->GetActorRotation()
		);
	}
	return;
}

void UGrabber::Release() {
	if (PhysicsHandle) { PhysicsHandle->ReleaseComponent(); }
	return;
}

// Called every frame
void UGrabber::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	if (!PhysicsHandle) { return; }
	if (PhysicsHandle->GrabbedComponent) {
		FVector ViewPoint = GetLineTraceEnd();
		PhysicsHandle->SetTargetLocation(ViewPoint);
	}
}

